@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <h4>{{$nivel1[0]->nivel1_nombre}}</h4>
                <div class="pl-5 pr-5">
                    <hr>
                </div>
            </div>
            <div class="col-md-12 text-center mb-5">
                {{$nivel1[0]->nivel1_descripcion}}
            </div>

            @forelse($nivel2 as $nivel)
                <div class="col-md-3">
                    <div class="card shadow-sm" style="min-height: 250px">
                        <div class="card-body">
                            {{$nivel->nivel2_nombre}}
                            <button class="btn btn-primary btn-block">sdd</button>
                        </div>
                    </div>
                </div>
            @empty
            @endforelse
        </div>
    </div>

@endsection
