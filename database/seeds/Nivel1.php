<?php

use Illuminate\Database\Seeder;

class Nivel1 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('nivel1s')->insert(array(
           'nivel1_nombre' => 'Descarte',
            'nivel1_descripcion' => 'No deben existir en el lugar de evaluación elementos extraños al proceso de producción.'
        ));
        DB::table('nivel1s')->insert(array(
            'nivel1_nombre' => 'Orden',
            'nivel1_descripcion' => 'Los elementos requeridos para la producción deben estar en el estacionamiento asignado. “CADA COSA EN SU LUGAR Y CADA LUGAR CON SU COSA”'
        ));
        DB::table('nivel1s')->insert(array(
            'nivel1_nombre' => 'Limpieza',
            'nivel1_descripcion' => 'Las áreas se deben encontrar limpias.'
        ));
        DB::table('nivel1s')->insert(array(
            'nivel1_nombre' => 'Estandarizacion'
        ));
        DB::table('nivel1s')->insert(array(
            'nivel1_nombre' => 'Disciplina',
            'nivel1_descripcion' => 'Las obligaciones y proceso de mejora continua son cumplidas de manera consistente creando la cultura organizacional buscada.'
        ));
    }
}
