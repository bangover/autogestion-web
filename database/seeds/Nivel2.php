<?php

use Illuminate\Database\Seeder;

class Nivel2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DESCARTE
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'Área de atención a clientes, acceso al banco, pasillos, escaleras y baño clientes.',
            'nivel1_id' => 1
        ));
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'Mezon atención a clientes.',
            'nivel1_id' => 1
        ));
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'Cubiculos de Ejecutivos de cuenta y Oficina Agente.',
            'nivel1_id' => 1
        ));
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'Áreas restrigidas.',
            'nivel1_id' => 1
        ));

        //ORDEN
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'Área de atención a clientes, acceso al banco, pasillos, escaleras y baño clientes.',
            'nivel1_id' => 2
        ));
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'Mezon atención a clientes.',
            'nivel1_id' => 2
        ));
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'Cubiculos de Ejecutivos de cuenta y Oficina Agente.',
            'nivel1_id' => 2
        ));
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'Áreas restrigidas.',
            'nivel1_id' => 2
        ));

        //LIMPIEZA
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'Las áreas se deben encontrar limpias.',
            'nivel1_id' => 3
        ));

        //ESTANDARIZACION
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'De las instalaciones.',
            'nivel1_id' => 4
        ));
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'Del personal.',
            'nivel1_id' => 4
        ));
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'Del Guardia de seguridad.',
            'nivel1_id' => 4
        ));
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'TBD.',
            'nivel1_id' => 4
        ));

        //DISCIPLINA
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'Autoevaluación diaria.',
            'nivel1_id' => 5
        ));
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'Protocolo de atención al cliente.',
            'nivel1_id' => 5
        ));
        DB::table('nivel2s')->insert(array(
            'nivel2_nombre' => 'TBD.',
            'nivel1_id' => 5
        ));
    }
}
