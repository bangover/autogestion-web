<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNivel2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nivel2s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nivel2_nombre');
            $table->integer('nivel1_id')->unsigned()->nullable();
            $table->foreign('nivel1_id')->references('id')->on('nivel1s');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nivel2s');
    }
}
