<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Nivel2
 *
 * @property-read \App\Nivel1 $nivel1
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Nivel3[] $nivel3
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nivel2 newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nivel2 newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nivel2 query()
 * @mixin \Eloquent
 */
class Nivel2 extends Model
{
    public function nivel1()
    {
        return $this->belongsTo('App\Nivel1');
    }
}
