<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Nivel1
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Nivel2[] $nivel2
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nivel1 newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nivel1 newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nivel1 query()
 * @mixin \Eloquent
 */
class Nivel1 extends Model
{
    public function nivel2()
    {
        return $this->hasMany('App\Nivel2');
    }
}
