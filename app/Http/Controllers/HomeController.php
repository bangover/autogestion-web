<?php

namespace App\Http\Controllers;

use App\Nivel1;
use Illuminate\Http\Request;

class HomeController extends Controller
{
//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        $nivel1 = Nivel1::with('nivel2')->get();
        //dd($nivel1);
        return view('home');
    }
}
