<?php

namespace App\Http\Controllers;

use App\Nivel1;
use App\Nivel2;
use Illuminate\Http\Request;
use Psy\Util\Json;

class NivelesController extends Controller
{
    public function Nivel1(Request $request){
        if ($request->ajax()){
            $nivel1 = Nivel1::with('nivel2')->get();
            return response()->json([
                'nivel1' => $nivel1
            ], 200);
        }
    }

    public function Nivel2(int $id){
        $nivel2 = Nivel2::where('nivel1_id', $id)->get();
        $nivel1 = Nivel1::where('id', $id)->get();
        return view('Nivel2.nivel2', compact('nivel2', 'nivel1'));
    }
}
